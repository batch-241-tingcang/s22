// Non-Mutator Methods
/*
- Non-mutator methods are functions that do not modify or change an array after they've been created. 
- These methods do not manipulate the original array. 
- These methods perform tasks such as returning(or getting) elements from an array, combining arrays, and printing the output 
- indexOf(), lastIndexOf(), slice(), toString(), concat(), join()
*/
console.log('%c' + 'Non-Mutator Methods', "font-weight: bold;")
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
console.log(countries);
console.log(" ")

// indexOf()
/*
- Returns the index number of the "first matching element" found in an array
- If no match was found, the result will be -1. 
- The search process will be done from first element proceeding to the last element
- Syntax:
    arrayName.indexOf(searchValue);
    arrayName.indexOf(searchValue, fromIndex);
*/
console.log("%c indexOf Method:", 'background: #89CFF0')
let firstIndex = countries.indexOf('PH')
console.log('Result of indexOf Method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Resut of indexOf Method: ' + invalidCountry);
console.log(" ")


// lastIndexOf()
/*
- Returns the index number of the "last matching item" found in an array
- The search process will be done from last element proceeding to the first element.
- Syntax: 
    arrayName.lastIndexOf(searchValue);
    arrayName.lastIndexOf(searchValue, fromIndex);
*/
console.log("%c lastIndexOf Method:", 'background: #89CFF0')

// Getting the index number starting from the last element
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);

// Getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf('PH', 6);
console.log('Result of lastIndexOf method: ' + lastIndexStart);
console.log(" ")


// slice()
/*
- Slices elements from an array AND returns a new array
- Syntax: 
    arrayName.slice(startingIndex);
    arrayName.slice(startingIndex, endingIndex)
*/
console.log("%c Slice Method:", 'background: #89CFF0')

// Slicing off elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log('Return from slice method:');
console.log(slicedArrayA);

// Slicing off elements from a specified index to another index
let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice method:');
console.log(slicedArrayB);

// Slicing off elements starting from the last element of an array
let slicedArrayC = countries.slice(-3);
console.log('Result from slice method:')
console.log(slicedArrayC);
console.log(" ")


// toString()
/*
- Returns an array as a string seperated by commas
- Syntax: 
    arrayName.toString()
*/
console.log("%c toString Method:", 'background: #89CFF0')
let stringArray = countries.toString();
console.log('Result from toString method:');
console.log(stringArray);
console.log(" ")


// concat()
/*
- Combines two arrays and returns the combined result
- Syntax: 
    arrayA.concat(arrayB);
    arrayA.concat(elementA);
*/
console.log("%c concat Method:", 'background: #89CFF0')
let tasksArrayA = ['drink HTML', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node']

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method:');
console.log(tasks);

// Combining multiple arrays
console.log('Result from concat method:');
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// Combining arrays with elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat method:');
console.log(combinedTasks);
console.log(" ")


// join()
/*
- Returns an array as a string seperated by specified seperator string
- Syntax: 
    arrayName.join('seperatorString');
*/
console.log("%c join Method:", 'background: #89CFF0')

let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));
console.log(" ")
console.warn("--end of non-mutator methods--")
console.log(" ")



console.log('%c' + 'Iteration Methods', "font-weight: bold;")
console.log(" ")
// Iteration Methods
/*
- Iteration methods are loops designed to perform repetitive tasks on arrays
- Useful for manipulating array data resulting in complex tasks
- foreach()
*/


// foreach()
/*
- Similar to the for loop that iterates on each array element
- Variable names for arrays are normally written in the plural form of the data stored in an array
- It's common practice to use the singular form of the array content for parameter names used in array loops
- Array iteration methods normally work with a function supplied as an argument
- How these function works is by performing tasks that are pre-defined within an array's method
- Syntax: 
    arrayName.forEach(function(individualElement){statement})
*/
console.log("%c forEach Method:", 'background: #89CFF0')
allTasks.forEach(function(task){console.log(task)});
console.log(" ")

// Using forEach with conditional statements
console.log("%c Filtered forEach:", "text-decoration: underline;")
let filteredTasks = [];

// Loops through all the tasks
/*
- It's good practice to print the current element when working with array iteration methods to have an idea of what information is being worked on for each iteration of the loop. 
- Creating a seperate variable to store results of array iteration methods are also good practice to avoid confusion by modifying the original array
*/
allTasks.forEach(function(task) {
    if(task.length > 10) {
        console.log(task)
        filteredTasks.push(task)
    };
});

console.log("Result of filtered tasks:");
console.log(filteredTasks);
console.log(" ")

// map()
/*
- Iterates on each element AND returns new array with different values depending on the result of the function's operation
- This is useful for perfoming tasks where mutating/changing the elements are required
- Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
- Syntax: 
    let/const resultArray = arrayName.map(function(indivElement))
*/
console.log("%c Map Method:", 'background: #89CFF0')
let numbers = [1, 2, 3, 4, 5];
console.log("Before the map method: " + numbers);

let numberMap = numbers.map(function(number) {
    return number * number
});

console.log("Result of map method:");
console.log(numberMap);
console.log(" ")

// every()
/*
- Checks if all element in an array meet the given condition
- This is useful for validating data stored in arrays especially when dealing with large amounts of data
- Returns a true value if all elements meet the condition and false if otherwise
- Syntax: 
    let/const resultArray = arrayName.every(function(indivElement) {
        return expression/condition;
    })
*/
console.log("%c Every Method:", 'background: #89CFF0')

let allValid = numbers.every(function(number) {
    return (number < 3);
});
console.log("Result of every method:");
console.log(allValid);
console.log(" ")


// some()
/*
- Checks if atleast one element in the array meets the given condition
- Returns a true value if atleast one element meets the condition and false if otherwise
- Syntax:
    let/const resultArray = arrayName.some(function(indivElement) {
        return expression/condition
    })
*/
let someValid = numbers.some(function(number) {
    return (number < 2);
});
console.log("Result of some method:");
console.log(someValid);

// Combining the returned result from the every/some method may be used in other statements to perform consecutive results
if (someValid) {
    console.log('Some numbers in the array are greater than 2');
}
console.log(" ")

// filter()
/*
- Returns new array that contains elements which meets the given condition
- Returns an empty array if no elements were fuond 
- Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods. 
- Syntax: 
    let / const resultArray = arrayName.filter(function(indivElement) {
        return expression / condition
    }) 
*/
console.log("%c Filter Method:", 'background: #89CFF0')

let filterValid = numbers.filter(function(number) {
    return (number <  3);
});
console.log("Result of filter method:");
console.log(filterValid);

// No elements found
let nothingFound = numbers.filter(function(number) {
    return (number = 0);
})
console.log("Result of filter method:");
console.log(nothingFound);

// Filtering using foreach
let filteredNumbers = [];

numbers.forEach(function(number){

    // console.log(number);

    if(number < 3) {
        filteredNumbers.push(number);
    }
})
console.log("Result of filter method:");
console.log(filteredNumbers);

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
console.log(" ")


// The includes method
/*
    - Methods can be "chained" using them one after another
    - The result of the first method is used on the second method until all "chained" methods have been resolved
    - How chaining resolves in our example:
        1. The "product" element will be converted into all lowercase letters
        2. The resulting lowercased string is used in the "includes" method
*/
console.log("%c Includes Method:", 'background: #89CFF0')

let filteredProducts = products.filter(function(product){
    return product.toLowerCase().includes('a');
})

console.log(filteredProducts);
console.log(" ")

// reduce() 
/* 
    - Evaluates elements from left to right and returns/reduces the array into a single value
    - Syntax
        let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
            return expression/operation
        })
    - The "accumulator" parameter in the function stores the result for every iteration of the loop
    - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
    - How the "reduce" method works
        1. The first/result element in the array is stored in the "accumulator" parameter
        2. The second/next element in the array is stored in the "currentValue" parameter
        3. An operation is performed on the two elements
        4. The loop repeats step 1-3 until all elements have been worked on
*/
console.log("%c reduce Method:", 'background: #89CFF0')

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y) {
    // Used to track the current iteration count and accumulator/currentValue data
    console.warn('current iteration: ' + ++iteration);
    console.log('accumulator: ' + x);
    console.log('currentValue: ' + y);

    // The operation to reduce the array into a single value
    return x + y;
});
console.log("Result of reduce method: " + reducedArray);

// reducing string arrays
let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x, y) {
    return x + ' ' + y;
});
console.log("Result of reduce method: " + reducedJoin);
